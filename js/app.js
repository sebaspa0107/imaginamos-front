//Clases
function Acciones(){

	var self = this;

	this.usuarios = false;
	this.companias = false;
	this.dataFiltro = false;
	this.usuariosOriginal = false;
	this.companiasOriginal = false;


	//Obtener todaa la data
	this.getAllData = function(){
		//$.when(self.getDataUsuarios(), self.getDataCompanias()).then(self.imprimeDataConsultada());
		$.ajax({
			url: 'data/users.json',
			type: 'POST',
			dataType: 'json',
			beforeSend: function(){
				$("#data-table").html('<tr><td colspan="5">Consultando...</td></tr>');
			},
			success: function(response){
				self.usuarios = response;
				self.usuariosOriginal = response;
				$.ajax({
					url: 'data/companies.json',
					type: 'POST',
					dataType: 'json',
					beforeSend: function(){
						$("#data-table").html('<tr><td colspan="5">Consultando...</td></tr>');
					},
					success: function(response){
						self.companias = response;
						self.companiasOriginal = response;

						self.imprimeDataConsultada();
						var opccompania = '';
						opccompania += '<option value="">Seleccione una compañia</option>';
						for(var i = 0; i < response.length; i++ ){
							opccompania += '<option value="'+response[i].id+'">'+response[i].name+'</option>';
						}
						$("#sel-compania").html(opccompania)
					},
					error: function(err){
						self.companias = false;
						console.error(err);
					}
				});
			},
			error: function(err){
				self.usuarios = false;
				console.error(err);
			}
		});
	}

	//Buscar el nombre de una compañia por su id
	this.findXCompania = function(idcompania){
		var fcompania = '';
		$.each(self.companias, function(i, v) {
        if (v.id == idcompania) {
            fcompania = v.name;
        }
    });
		return fcompania;
	}

	//Imprimir la data consultada
	this.imprimeDataConsultada = function(){
		if(self.usuarios == false || self.companias == false){
			console.error("No hay data para consultar");
			$("#data-table").html('<tr><td colspan="5">No hay datos para mostrar...</td></tr>');
		}
		else{
			$("#data-table").html('');

			var dataTable = '';

			for(var i = 0; i < self.usuarios.length; i++ ){
				dataTable += '<tr>';
				dataTable += '<td>'+self.usuarios[i].name+'</td>';
				dataTable += '<td>'+self.usuarios[i].email+'</td>';
				dataTable += '<td>'+self.findXCompania(self.usuarios[i].company_id)+'</td>';
				dataTable += '<td>'+self.usuarios[i].phone+'</td>';
				dataTable += '<td> <a href="http://'+self.usuarios[i].website+'" target="_blank">'+self.usuarios[i].website+'</a></td>';
				dataTable += '</tr>';
			}

			$("#data-table").append(dataTable);
		}
	};

	this.filtraPorCampo = function(campo, valor){
		valor = valor.trim();
		self.dataFiltro = JSON.search( self.usuariosOriginal, '//*['+campo+'="'+valor+'"]' );

		self.usuarios = self.dataFiltro;
		self.imprimeDataConsultada();

	}

	this.filtartPorCompania = function(valor){
		self.dataFiltro = JSON.search( self.usuariosOriginal, '//*[company_id="'+valor+'"]' );

		self.usuarios = self.dataFiltro;
		self.imprimeDataConsultada();
	}

	//Inicializar los valores de la data consultada
	this.inicializarData = function(){
		self.getAllData();
	}

}

//Variables para instanciar las clases
var acciones = new Acciones();

//Inicializar las variables con la data obtenida
acciones.inicializarData();

//Formulario de busqueda para buscar por cada campo
$("#form-buscar").on("submit", function(){
	var input_nombre = $("#nombre").val();
	var input_email = $("#email").val();
	var input_telefono = $("#telefono").val();
	var input_sitioweb = $("#sitioweb").val();

	if(input_nombre != ""){
		acciones.filtraPorCampo('name',input_nombre);
	}
	if(input_email != ""){
		acciones.filtraPorCampo('email',input_email);
	}
	if(input_telefono != ""){
		acciones.filtraPorCampo('phone',input_telefono);
	}
	if(input_sitioweb != ""){
		acciones.filtraPorCampo('website',input_sitioweb);
	}

	if(input_nombre == "" && input_email == "" && input_telefono == "" && input_sitioweb == ""){
		acciones.inicializarData();
	}


	return false;
});

//Buscar al seleccionar alguna compañia
$("#sel-compania").on("change", function(){
	if($(this).val() == ""){
		acciones.inicializarData();
	}
	else{
		var idcompania = $(this).val();
		acciones.filtartPorCompania(idcompania);
	}

});
